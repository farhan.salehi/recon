#!/bin/bash

if [ -z "$1" ]; then
    echo "Usage: $0 <domain>"
    exit 1
fi

domain="$1"
output_dir="${domain}_subdomains"

# Create the output directory if it doesn't exist
mkdir -p "$output_dir"

echo "Running subdomain discovery for $domain..."

# Use assetfinder to discover subdomains
assetfinder --subs-only $domain > "$output_dir/${domain}_assetfinder_subs.txt"

# Use gau to discover URLs and extract subdomains
gau $domain | cut -d '/' -f 3 | sort -u > "$output_dir/${domain}_gau_subs.txt"

# Use waybackurls to discover URLs and extract subdomains
waybackurls $domain | awk -F/ '{print $3}' | sort -u > "$output_dir/${domain}_wayback_subs.txt"

echo "Running httprobe to check for live subdomains..."

# Combine and deduplicate subdomains from all sources, then check live status
(cat "$output_dir/${domain}_assetfinder_subs.txt" "$output_dir/${domain}_gau_subs.txt" "$output_dir/${domain}_wayback_subs.txt" | sort -u | httprobe | sort -u) > "$output_dir/${domain}_all_alive_subdomains.txt"

echo "Results saved in $output_dir/${domain}_all_alive_subdomains.txt"
